package chat_client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterUI extends JFrame {
	private JFrame frame = new JFrame();
	private JTextField tf_regist_username = new JTextField();
	private JTextField tf_regist_password = new JTextField();
	private final JLabel lblPassword = new JLabel("PASSWORD : ");
	private final JLabel lblAccount = new JLabel("ACCOUNT : ");
	private final JButton btnRegist = new JButton("Regist");
	private final JButton btnCancel = new JButton("Cancel");
	

	public RegisterUI() {
		frame.getContentPane().setFont(new Font("Yu Gothic Medium", Font.PLAIN, 11));
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setBounds(400,250,500,300);
		frame.setTitle("Register");
		
		Container con = frame.getContentPane();
		frame.getContentPane().setLayout(null);
		tf_regist_username.setBounds(154,63,298,26);
		con.add(tf_regist_username);
		tf_regist_password.setBounds(154,111,298,26);
		con.add(tf_regist_password);
		
		JLabel lblRegist = new JLabel("REGISTER ACCOUNT");
		lblRegist.setForeground(Color.DARK_GRAY);
		lblRegist.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 24));
		lblRegist.setBounds(134, 11, 235, 26);
		frame.getContentPane().add(lblRegist);
		
		lblAccount.setFont(new Font("Sitka Subheading", Font.BOLD, 15));
		lblAccount.setBounds(38, 58, 106, 36);
		frame.getContentPane().add(lblAccount);
		
		lblPassword.setFont(new Font("Sitka Subheading", Font.BOLD, 15));
		lblPassword.setBounds(38, 106, 95, 36);
		frame.getContentPane().add(lblPassword);
		
		JButton btnRegist = new JButton("OK");
		btnRegist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = tf_regist_username.getText();
				String password = tf_regist_password.getText();
				accounts account = new accounts();
				account.create_new_user(username, password);
				
				frame.setVisible(false);
			}
		});
		btnRegist.setBounds(89, 208, 89, 23);
		frame.getContentPane().add(btnRegist);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnCancel.setBounds(294, 208, 89, 23);
		frame.getContentPane().add(btnCancel);
		
		
		
	}
}
