package chat_client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class accounts {
	private static final String ACCOUNT_FILE = "./client_accounts.txt";

	private static FileReader read_file(String file_name) {
		FileReader file = null;
		try {
			file = new FileReader(file_name);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return file;
	}
	
	private static void write_file(String file_name,String writeString) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(file_name,true));
			writer.write(writeString);
			writer.newLine();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

 	public static boolean check_accounts(String username, String password) throws IOException {
		List<String> usernames_list = new ArrayList<String>();
		List<String> passwords_list = new ArrayList<String>();

		FileReader file = read_file(ACCOUNT_FILE);
		BufferedReader buffReader = null;

		try {

			buffReader = new BufferedReader(file);

			String currentLine;

			while ((currentLine = buffReader.readLine()) != null) {
				String[] account = currentLine.split("\t\t\t");
				usernames_list.add(account[0]);
				passwords_list.add(account[1]);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < usernames_list.size(); i++) {
			if ((username.equals(usernames_list.get(i))) && (password.equals(passwords_list.get(i)))) {
				return true;
			}
		}
		return false;
	}

	public void create_new_user(String username, String password) {
		String writeString = username + "\t\t\t" + password;
		write_file(ACCOUNT_FILE,writeString);
	}
}
