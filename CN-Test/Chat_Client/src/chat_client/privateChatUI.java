package chat_client;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class privateChatUI {
	private JFrame frame = new JFrame();
	public static JTextField tfOtherUser;
	private JButton btnNewButton;
	private JButton btnClose;
	
	privateChatUI(ArrayList<String> users) {
		frame.getContentPane().setFont(new Font("Yu Gothic Medium", Font.PLAIN, 11));
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setBounds(400,250,500,300);
		frame.setTitle("Private Chat");
		
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(10, 49, 229, 201);
		frame.getContentPane().add(textArea);
		int i = 1;
		for (String current_user : users) {
			textArea.append(i + ". " + current_user + "\n");
			i++;
		}
		textArea.setEditable(false);
		
		
		JLabel lblNewLabel = new JLabel("Online Users");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(67, 24, 109, 14);
		frame.getContentPane().add(lblNewLabel);
		
		tfOtherUser = new JTextField();
		tfOtherUser.setBounds(287, 51, 143, 20);
		frame.getContentPane().add(tfOtherUser);
		tfOtherUser.setColumns(10);
		
		JLabel lblEnterUsername = new JLabel("Enter Username :");
		lblEnterUsername.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblEnterUsername.setBounds(287, 24, 143, 14);
		frame.getContentPane().add(lblEnterUsername);
		
		btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				frame.setVisible(false);
			}
		});
		btnNewButton.setBounds(249, 148, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnClose.setBounds(385, 148, 89, 23);
		frame.getContentPane().add(btnClose);
		
	}
}
