package chat_client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

public class client_frame extends javax.swing.JFrame {
	String username, password;
	String address = "localhost";
	String otherUser = null;
	int port = 2222;
	ArrayList<String> users = new ArrayList();

	Boolean isConnected = false;
	Boolean isSwitch = false;

	Socket sock;
	BufferedReader reader;
	PrintWriter writer;
	byte by[] = new byte[2002];
	privateChatUI priChat;

	// --------------------------//

	public void ListenThread() {
		Thread IncomingReader = new Thread(new IncomingReader());
		IncomingReader.start();
	}

	// --------------------------//

	public void userAdd(String data) {
		users.add(data);
	}

	// --------------------------//

	public void userRemove(String data) {
		ta_chat.append(data + " is now offline.\n");
	}

	// --------------------------//

	public void writeUsers() {
		String[] tempList = new String[(users.size())];
		users.toArray(tempList);
		for (String token : tempList) {
			// users.append(token + "\n");
		}
	}

	// --------------------------//

	public void sendDisconnect() {
		String bye = (username + ": :Disconnect");
		try {
			writer.println(bye);
			writer.flush();
		} catch (Exception e) {
			ta_chat.append("Could not send Disconnect message.\n");
		}
	}

	// --------------------------//

	public void Disconnect() {
		try {
			ta_chat.append("Disconnected.\n");
			sock.close();
		} catch (Exception ex) {
			ta_chat.append("Failed to disconnect. \n");
		}
		isConnected = false;
		tf_username.setEditable(true);
		tf_password.setEditable(true);
		tf_address.setEditable(true);
		tf_port.setEditable(true);

	}

	public void sendFile(String file) throws IOException {
		DataOutputStream dos = new DataOutputStream(sock.getOutputStream());
//		InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
		FileInputStream fis = new FileInputStream(file);
		byte[] buffer = new byte[4096];

		while (fis.read(buffer) > 0) {
			dos.write(buffer);
		}

		dos.close();
		fis.close();

	}

	public client_frame() {
		initComponents();
	}

	// --------------------------//

	public class IncomingReader implements Runnable {
		@Override
		public void run() {
			String[] data;
			String stream, done = "Done", connect = "Connect", disconnect = "Disconnect", chat = "Chat", file = "File";
			String currentUser = null;
			
			try {
				while ((stream = reader.readLine()) != null) {
					if (isSwitch == false) {
						currentUser = username;
					}
					data = stream.split(":");
					
					if (!data[data.length-1].equals(chat) && 
							!data[data.length-1].equals(file) &&
							!data[data.length-1].equals(connect) &&
							!data[data.length-1].equals(disconnect))
					{
						if (username.equals(data[data.length-1])) {
							ta_chat.append(data[0] + ": " + data[1] + "\n");
							ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
						}
						if (username.equals(data[0]) && !data[1].equals("")) {
							ta_chat.append(data[0] + ": " + data[1] + "\n");
							ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
						}
					}
					else if (data[2].equals(chat)) {
						ta_chat.append(data[0] + ": " + data[1] + "\n");
						ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
					} else if (data[2].equals(file)) {
						ta_chat.append(data[0] + ": " + data[1] + "\n");
						ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
					} else if (data[2].equals(connect)) {
						ta_chat.removeAll();
						userAdd(data[0]);
					} else if (data[2].equals(disconnect)) {
						userRemove(data[0]);
					} else if (data[2].equals(done)) {
						// users.setText("");
						writeUsers();
						users.clear();
					}
				}
			} catch (Exception ex) {
			}
		}
	}

	// --------------------------//

	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated
	// Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		lb_address = new JLabel();
		tf_address = new JTextField();
		lb_port = new JLabel();
		tf_port = new JTextField();
		lb_username = new JLabel();
		tf_username = new JTextField();
		lb_password = new JLabel();
		tf_password = new JTextField();
		b_connect = new JButton();
		b_disconnect = new JButton();
		b_regist = new JButton();
		jScrollPane1 = new JScrollPane();
		tf_chat = new JTextField();
		b_send = new JButton();
		lb_name = new JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setTitle("Chat - Client's frame");
		setName("client"); // NOI18N
		setResizable(false);

		lb_address.setText("Address : ");

		tf_address.setText("localhost");
		tf_address.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tf_addressActionPerformed(evt);
			}
		});

		lb_port.setText("Port :");

		tf_port.setText("2222");
		tf_port.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tf_portActionPerformed(evt);
			}
		});

		lb_username.setText("Username :");

		tf_username.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				tf_usernameActionPerformed(evt);
			}
		});

		lb_password.setText("Password : ");

		b_connect.setText("Connect");
		b_connect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try {
					b_connectActionPerformed(evt);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		b_disconnect.setText("Disconnect");
		b_disconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_disconnectActionPerformed(evt);
			}
		});

		b_regist.setText("Register");
		b_regist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_registerActionPerformed(evt);
			}
		});

		b_send.setText("Send Message");
		b_send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_sendActionPerformed(evt);
			}
		});

		lb_name.setText(" Chat Application powered by W�n ");
		lb_name.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

		fileNameField = new JTextField();
		fileNameField.setText("Click \"Open File ... \"");
		fileNameField.setColumns(10);

		inputFilebtn = new JButton();
		inputFilebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_getFileActionPerformed(evt);
			}
		});
		inputFilebtn.setText("Open file...");

		JButton sendFilebtn = new JButton();
		sendFilebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_sendFileActionPerformed(evt);
			}
		});
		sendFilebtn.setText("Send file");
		ta_chat = new javax.swing.JTextArea();

		ta_chat.setColumns(20);
		ta_chat.setRows(5);
		
		btnPublicMode = new JButton();
		btnPublicMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				isSwitch = false;
			}
		});
		btnPublicMode.setText("PUBLIC MODE");
		
		btnPrivateMode = new JButton();
		btnPrivateMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				b_privateChatActionPerformed(evt,otherUser);
			}
		});
		btnPrivateMode.setText("PRIVATE MODE");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addContainerGap()
							.addGroup(layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(lb_username, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
										.addComponent(lb_address, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(18)
									.addGroup(layout.createParallelGroup(Alignment.LEADING)
										.addComponent(tf_address, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
										.addComponent(tf_username, 103, 103, 103))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(layout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(lb_password, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(lb_port, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(layout.createParallelGroup(Alignment.LEADING)
										.addComponent(tf_port, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
										.addComponent(tf_password, 50, 50, 50))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(layout.createParallelGroup(Alignment.LEADING, false)
										.addGroup(layout.createSequentialGroup()
											.addComponent(b_connect)
											.addGap(2)
											.addComponent(b_disconnect))
										.addComponent(b_regist, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(0, 4, Short.MAX_VALUE))
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup(Alignment.TRAILING)
										.addGroup(layout.createSequentialGroup()
											.addComponent(fileNameField, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE)
											.addGap(18)
											.addComponent(inputFilebtn, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(sendFilebtn, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
										.addGroup(layout.createSequentialGroup()
											.addComponent(tf_chat, GroupLayout.PREFERRED_SIZE, 352, GroupLayout.PREFERRED_SIZE)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(b_send, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
										.addGroup(layout.createSequentialGroup()
											.addGroup(layout.createParallelGroup(Alignment.TRAILING)
												.addGroup(layout.createSequentialGroup()
													.addComponent(ta_chat, GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
													.addPreferredGap(ComponentPlacement.RELATED))
												.addGroup(Alignment.LEADING, layout.createSequentialGroup()
													.addGap(65)
													.addComponent(btnPublicMode, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
													.addGap(70)
													.addComponent(btnPrivateMode, GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
													.addGap(100)))
											.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
									.addGap(12))))
						.addGroup(layout.createSequentialGroup()
							.addGap(149)
							.addComponent(lb_name)))
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lb_address)
						.addComponent(tf_address, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lb_port)
						.addComponent(tf_port, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(b_regist))
					.addGap(18)
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(lb_username)
							.addComponent(lb_password)
							.addComponent(b_connect)
							.addComponent(b_disconnect)
							.addComponent(tf_password, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(tf_username, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnPrivateMode)
								.addComponent(btnPublicMode))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(ta_chat, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE))
						.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(tf_chat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(b_send))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(fileNameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(sendFilebtn)
						.addComponent(inputFilebtn))
					.addGap(7)
					.addComponent(lb_name, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>//GEN-END:initComponents

	private void tf_addressActionPerformed(ActionEvent evt) {

	}

	private void tf_portActionPerformed(ActionEvent evt) {

	}

	private void tf_usernameActionPerformed(ActionEvent evt) {

	}

	// Event button to check insert account
	private void b_connectActionPerformed(ActionEvent evt) throws IOException {
		boolean check_account;
		if (isConnected == false) {
			username = tf_username.getText();
			password = tf_password.getText();
			check_account = accounts.check_accounts(username, password);

			if (!check_account) {
				ta_chat.append("Your account is invalid ! Try Again. \n");
				ta_chat.append("Click Register to create new account. \n");
			} else {
				try {
					address = tf_address.getText();
					sock = new Socket(address, port);
					
					InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
					
					reader = new BufferedReader(streamreader);
					writer = new PrintWriter(sock.getOutputStream());
					writer.println(username + ":has connected.:Connect");
					writer.flush();
					
					ta_chat.append(username + " connected to the server. \n");
					tf_username.setEditable(false);
					tf_password.setEditable(false);
					tf_address.setEditable(false);
					tf_port.setEditable(false);

					isConnected = true;
				} catch (Exception ex) {
					System.out.println("Error : " + ex);
					ta_chat.append("Cannot Connect this server ! Try Again. \n");
				}

			}

			ListenThread();
		} else if (isConnected == true) {
			ta_chat.append("You are already connected. \n");
		}
	}

	// Event button to disconnect
	private void b_disconnectActionPerformed(ActionEvent evt) {
		sendDisconnect();
		Disconnect();
	}

	// Event button to regist new account
	private void b_registerActionPerformed(ActionEvent evt) {
		RegisterUI viewCourseGUI = new RegisterUI();

	}
	
	private void  b_privateChatActionPerformed(ActionEvent evt,String otherUser1) {
		priChat = new privateChatUI(users);
		isSwitch = true;
	}

	// Event button to send chat message
	private void b_sendActionPerformed(ActionEvent evt) {
		String nothing = "";
		otherUser = priChat.tfOtherUser.getText();
//		if (otherUser.equals("")) {
//			isSwitch = false;
//		}
		if ((tf_chat.getText()).equals(nothing)) {
			tf_chat.setText("");
			tf_chat.requestFocus();
		} else {
			try {
				if (isSwitch == false) {
					writer.println(username + ":" + tf_chat.getText() + ":" + "Chat");
					writer.flush(); // flushes the buffer
				} else {
					writer.println(username + ":" + tf_chat.getText() + ":" + "Chat" + ":" + otherUser);
					writer.flush(); // flushes the buffer
				}
			} catch (Exception ex) {
				ta_chat.append("Message was not sent. \n");
			}
			tf_chat.setText("");
			tf_chat.requestFocus();
		}

		tf_chat.setText("");
		tf_chat.requestFocus();
	}

	// Event button to get file name
	private void b_getFileActionPerformed(ActionEvent evt) {
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(fc);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			String fileName = file.getAbsolutePath();
			fileNameField.setText(fileName);
		}
	}

	private void b_sendFileActionPerformed(ActionEvent evt) {
		String nothing = "";
		String stringOpen = "Click \"Open File ... \"";
		if ((fileNameField.getText()).equals(nothing) || (fileNameField.getText()).equals(stringOpen)) {
			fileNameField.setText(stringOpen);
			fileNameField.requestFocus();
		} else {
			try {
				String fileName = fileNameField.getText();
				String abFileName = fileName.replace("\\", "\\\\");
				String part[] = abFileName.split("\\\\");

				writer.println(username + ":" + part[part.length - 1] + ":" + "File");
				writer.flush(); // flushes the buffer

				sendFile(fileName);
			} catch (Exception ex) {
				ta_chat.append("File could not send. \n");
			}
			fileNameField.setText(stringOpen);
			fileNameField.requestFocus();
		}

		fileNameField.setText(stringOpen);
		fileNameField.requestFocus();
	}

	public static void main(String args[]) throws IOException {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				new client_frame().setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify
	private JButton b_regist;
	private JButton b_connect;
	private JButton b_disconnect;
	private JButton b_send;
	private JButton inputFilebtn;
	private JButton btnPrivateMode;
	private JButton btnPublicMode;
	private JScrollPane jScrollPane1;
	private JLabel lb_address;
	private JLabel lb_name;
	private JLabel lb_password;
	private JLabel lb_port;
	private JLabel lb_username;
	private JTextArea ta_chat;
	private JTextField tf_address;
	private JTextField tf_chat;
	private JTextField tf_password;
	private JTextField tf_port;
	private JTextField tf_username;
	private JTextField fileNameField;
}
